# coding: utf-8
import json
from loremipsum import *
from random import randint, choice
from hashlib import sha256
from base64 import urlsafe_b64encode as b64enc

RANDOM_IMAGES = '''http://i.imgur.com/uhv2Ifu.jpg
http://i.imgur.com/cRUIOYz.jpg
http://i.imgur.com/Tx5iVXo.gif
http://i.imgur.com/LcMPDV6.gif
http://i.imgur.com/NS0lq86.gif
http://i.imgur.com/MLmoBDB.jpg
http://i.imgur.com/tkayLBJ.jpg
http://i.imgur.com/2TD1KMP.jpg
http://i.imgur.com/OjiDn5A.jpg
http://i.imgur.com/K8GrU4X.gif
http://i.imgur.com/PxLcgSp.png
http://i.imgur.com/3X7Z1uV.jpg
http://i.imgur.com/NPwJL91.jpg
http://i.imgur.com/oDoow0q.png
http://i.imgur.com/dpQtN0j.png
http://i.imgur.com/c9nIGNK.png'''.splitlines()

def threads():
    d = {'i': 0}
    def gen():
        x = {'model': 'app.Thread', 'pk': d['i'], 'fields': {}}
        d['i'] += 1
        return x
    return gen

def posts():
    d = {'i': 0}
    def gen(thread):
        x = {
        'model': 'app.Post',
        'pk': d['i'],
        'fields': {
        	'text': '\n'.join(get_sentences(randint(1,10))),
        	'img_url': choice(RANDOM_IMAGES),
        	'thread': thread['pk'],
        	'submitter': b64enc(sha256(str(randint(0,2**32))).digest())[:43],
            'date': '2014-07-%02d %02d:%02d:%02d+00:00' % (
                randint(14, 15),
                randint(0, 23),
                randint(0, 59),
                randint(0, 59),
            ),
        }
        }
        d['i'] += 1
        return x
    return gen

gen = threads()
t = [gen() for _ in range(4)]

p = []
    
gen = posts()
for thread in t:
    p += [gen(thread) for _ in range(5)]
    
with open('fixtures/initial_data.json', 'w') as f:
    json.dump(p+t, f, indent=4)
