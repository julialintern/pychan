#!/usr/bin/env python

ALLOWED_HOSTS = (
        '127.0.0.1',
        'localhost',
)

DEBUG = False
