from django.conf.urls import patterns, include, url

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pychan.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^$', 'app.views.home', name='home'),
    url(r'^page/(?P<page_num>[0-9]+)/$', 'app.views.home', name='home'),
    url(r'^thread/(?P<pk>[0-9]+)/$', 'app.views.thread', 
        name='thread'),
    url(r'^post/(?P<pk>[0-9]*)$', 'app.views.post',
        name='post'),
)
