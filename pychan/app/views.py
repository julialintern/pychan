import re
from datetime import datetime
from hashlib import sha256
from base64 import urlsafe_b64encode as b64enc

from django.shortcuts import render, redirect
from django.core.paginator import Paginator
from django.core.context_processors import csrf
from django.core.exceptions import ObjectDoesNotExist

from .models import Post, Thread

RE_IMG_URL = re.compile(r'^(https?:\/\/.*\.(?:png|jpg|gif|jpeg))$',
    re.IGNORECASE)

HITS = 0
KILL_AFTER = 1000000

def check_kill():
    global HITS
    HITS += 1
    if HITS % 10000:
        return
    posts = Post.objects.all().order_by('-pk')[:KILL_AFTER]
    Post.objects.exclude(pk__in=posts).delete()
    threads = set([p.thread.pk for p in Post.objects.all()])
    Thread.objects.exclude(pk__in=threads).delete()

def home(request, page_num=1):
    page_num = int(page_num)
    threads = Thread.objects.all().order_by('-id')
    paginator = Paginator(threads, 10)
    page = paginator.page(page_num)
    thread_objs = paginator.page(page_num).object_list
    thread_posts = []
    for obj in thread_objs:
        # The last two which aren't OP ~ nasty query...
        posts = obj.posts.exclude(pk__exact=obj.op().pk).order_by('-id')[:2]
        thread_posts += [(obj, posts)]
    disabled = {
            'first': not page.has_previous(),
            'prev': not page.has_previous(),
            'next': not page.has_next(),
            'last': not page.has_next(),
    }
    pages = {
            'prev': page_num - 1 if not disabled['prev'] else page_num,
            'next': page_num + 1 if not disabled['next'] else page_num,
            'last': paginator.page(paginator.num_pages).number if not disabled['next'] else page_num,
    }
    context = {
        'disabled': disabled,
        'threads': thread_posts,
        'pages': pages,
    }
    context.update(csrf(request))
    return render(request, 'home.html', context)

def post(request, pk):
    check_kill()
    if request.method == 'POST':
        ptext = (
            request.META.get('HTTP_X_REAL_IP', '') +
            request.COOKIES.get('csrftoken')
        )
        submitter = b64enc(sha256(ptext).digest())[:43]
        img_url = request.POST.get('img_url', '')
        msg = request.POST.get('msg', '')
        if not pk:
            if not img_url:
                raise ValueError('image required')
            elif not RE_IMG_URL.match(img_url):
                raise ValueError('not an image URL')
            thread = None
        else:
            thread = Thread.objects.get(pk=int(pk))
        if img_url and not RE_IMG_URL.match(img_url):
            raise ValueError('not an image URL')
        reply = Post(text=msg, date=datetime.now(), submitter=submitter,
            src=request.META.get('HTTP_X_REAL_IP', 'none'))
        if img_url:
            success = reply.load_img(img_url)
            reply.img.save()
            if not success:
                raise ValueError('Too large')
        if not thread:
            thread = Thread.objects.create()
        reply.thread = thread
        reply.save()
        return redirect('thread', thread.pk)
    else:
        return redirect('home')

def thread(request, pk):
    thread = Thread.objects.get(pk=pk)
    context = {'posts': thread.posts.all().order_by('id')}
    context.update(csrf(request))
    return render(request, 'thread.html', context)
